SERVER_URI="http://ngagement.nguvu.com"
APP_NAME="ngagement-service"
USERNAME="braunson.lalonde"
PASSWORD="Nguvu;12345"

curl_request(){

	local CURL_REQUEST_URL=$1
	local DISPLAY_MSG=$2
	local ENABLE_COOKIES=$3
	local METHOD=$4
	local POSTDATA=$5

	local COOKIE_PARAMS=""
	if [ "$ENABLE_COOKIES" -eq 1 ] ; then
		local COOKIE_PARAMS="-c cookies.txt -b cookies.txt"
	fi

	local METHOD_PARAMS=""
	if [ ! -z "$METHOD" ] ; then
		local METHOD_PARAMS="-X $METHOD"
	fi

	local POSTDATA_PARAMS=""
	if [ ! -z "$POSTDATA" ] ; then
		local POSTDATA_PARAMS="--data $POSTDATA"
	fi
	
	if [ "$DISPLAY_MSG" -eq 1 ] ; then
		echo "---------------------------------------------"
		echo "Sending curl request..." 
		echo "---------------------------------------------"
		echo $CURL_REQUEST_URL
		if [ ! -z "$POSTDATA" ] ; then
			echo $POSTDATA | python -m json.tool
		fi
		echo "---------------------------------------------"
	fi

	#echo "METHOD_PARAMS:" $METHOD_PARAMS
	#echo "METHOD:" $METHOD

	OUT=$( curl -w '\n%{http_code}' $POSTDATA_PARAMS $COOKIE_PARAMS $METHOD_PARAMS $CURL_REQUEST_URL ) 2>/dev/null

	if [ "$DISPLAY_MSG" -eq 1 ] ; then
		echo "HTTP Code:" 
		echo "$OUT" | tail -n1
		echo "Response is:"
		echo "\t$OUT" | head -n-1 | python -m json.tool
		echo "---------------------------------------------"
	fi
}

login_user(){
	local USERNAME=$1
	echo "Logging in as: " $USERNAME
	REQUEST=$SERVER_URI"/"$APP_NAME"/""session?""username="$USERNAME"&password="$PASSWORD
	curl_request $REQUEST 0 1
}

logout_user(){	
	echo "Logging out: " $USERNAME
	REQUEST=$SERVER_URI"/"$APP_NAME"/""session"
	curl_request $REQUEST 0 1 "DELETE"
}

display_choice(){
	echo "---------------------------------------------"
	echo "Please enter choice:-"
	echo "---------------------------------------------"
	echo "\t0. EXIT"
	echo
	echo "\t1. GET /session success"
	echo "\t2. GET /session failure"
	echo
	echo "\t3. GET /status success"
	echo "\t4. GET /status failure"
	echo
	echo "\t5. GET /performance success"
	echo
	echo "\t6. GET /challenges"
	echo "\t7. POST /challenges"
	echo "\t8. PUT /challenges"
	echo
	echo "\t9. GET /goals"
	echo "\t10. POST /goals"
	echo "\t11. PUT /goals"
	echo "---------------------------------------------"	

	echo "\t100. LOGIN"
	echo "\t101. LOGOUT"
	
	echo "---------------------------------------------"

	read CHOICE

	DISPLAY_MSG=1	
	ENABLE_COOKIES=0
	METHOD="GET"
	POSTDATA=""
	SEND_REQ=1

	case $CHOICE in
		0)  exit ;;

		1)  REQUEST=$SERVER_URI"/"$APP_NAME"/""session?""username="$USERNAME"&password="$PASSWORD;;
          
		2)  REQUEST=$SERVER_URI"/"$APP_NAME"/""session?""username="$USERNAME"&password=INVALID";;
			
		3)  ENABLE_COOKIES=1
			REQUEST=$SERVER_URI"/"$APP_NAME"/""status?";;

		4)  ENABLE_COOKIES=0
			REQUEST=$SERVER_URI"/"$APP_NAME"/""status?"
			;;

		5)  ENABLE_COOKIES=1
			REQUEST=$SERVER_URI"/"$APP_NAME"/""performance?";;

		6)  ENABLE_COOKIES=1
			REQUEST=$SERVER_URI"/"$APP_NAME"/""challenges?";;

		7)  echo "Enter challengee Id:"
			read CHALLENGEE_ID
			echo "Enter duration:"
			read DURATION

			ENABLE_COOKIES=1
			METHOD="POST"
			POSTDATA="{\"challengee\":\"$CHALLENGEE_ID\",\"duration\":\"$DURATION\"}"
			REQUEST=$SERVER_URI"/"$APP_NAME"/""challenges?";;

		8)  echo "Enter challenge Id:"
			read CHALLENGE_ID
			echo "Enter creator Id:"
			read CREATOR_ID
			echo "Enter participant Id:"
			read PARTICIPANT_ID
			echo "Enter participant response:"
			read PARTICIPANT_RESPONSE

			ENABLE_COOKIES=1
			METHOD="PUT"			
			POSTDATA="{\"id\":\"$CHALLENGE_ID\",\"creator\":{\"id\":\"$CREATOR_ID\"},\"participants\":[{\"id\":\"$PARTICIPANT_ID\",\"response\":\"$PARTICIPANT_RESPONSE\"}]}"
			REQUEST=$SERVER_URI"/"$APP_NAME"/""challenges?";;

		9)  ENABLE_COOKIES=1
			REQUEST=$SERVER_URI"/"$APP_NAME"/""goals?";;

		10) echo "Enter participant Id:"
			read PARTICIPANT_ID
			echo "Enter duration:"
			read DURATION

			ENABLE_COOKIES=1
			METHOD="POST"
			POSTDATA="{\"workDaysPerParticipant\":\"$DURATION\",\"participants\":[{\"profile\":{\"id\":\"$PARTICIPANT_ID\"}}]}"
			REQUEST=$SERVER_URI"/"$APP_NAME"/""goals?";;

		11)	echo "Enter goal Id:"
			read GOAL_ID
			echo "Enter creator Id:"
			read CREATOR_ID
			echo "Enter participant Id:"
			read PARTICIPANT_ID
			echo "Enter participant response:"
			read PARTICIPANT_RESPONSE

			ENABLE_COOKIES=1
			METHOD="PUT"			
			POSTDATA="{\"id\":\"$GOAL_ID\",\"creator\":{\"id\":\"$CREATOR_ID\"},\"participants\":[{\"profile\":{\"id\":\"$PARTICIPANT_ID\"},\"response\":\"$PARTICIPANT_RESPONSE\"}]}"
			REQUEST=$SERVER_URI"/"$APP_NAME"/""goals?";;

		100)echo "Please enter username:"
			read USERNAME_INPUT
			USERNAME=$USERNAME_INPUT
			login_user $USERNAME
			SEND_REQ=0;;

		101)logout_user
			SEND_REQ=0;;

		*) echo "INVALID CHOICE" 
		   display_choice;;

	esac

		if [ "$SEND_REQ" -eq 1 ] ; then
			curl_request $REQUEST $DISPLAY_MSG $ENABLE_COOKIES $METHOD $POSTDATA
		fi

		display_choice
}

display_choice


